# Prerequisites

If you haven't already:

- Install [sdkman](https://sdkman.io/) and use it to install the latest version of Java from your favorite distro of the JDK (Zulu, etc).
    - On Linux, install git with your OS's package manager.
    - On OS X, installing XCode through the App Store is probably the easiest way to get git.
    - On Windows, there is an installer for git available in the link above.
    - If you want a graphical git tool, consider [SourceTree](https://www.sourcetreeapp.com/) for Mac & Windows. For Linux, there are many options, but [gitg](https://git.gnome.org/browse/gitg) isn't bad.

Now, you can either use this project as is, or learn how to make something like it from scratch yourself.

# I just want to write some Java
## AKA, someone on IRC said to go here

This project will get you up to speed with a grown-up project structure.

Clone the repository:

```
git clone https://marshallpierce@bitbucket.org/marshallpierce/java-quickstart.git your-project-name
cd your-project-name
```

From now on, the commands assume that you're in the newly cloned project directory.

Remove the git repo and make a new one so that you don't pollute your project with the history of this quickstart sample.

```
rm -rf .git
git init
git add .
git commit -am "Initial commit"
```

Run the build (use `gradlew.bat` if on windows):
```
./gradlew build
```

## Configure gradle for your environment

If you are behind a proxy, you'll need to add lines to `~/.gradle/gradle.properties` as [described here](https://docs.gradle.org/current/userguide/build_environment.html).

## Import the project into IntelliJ
- If you have an existing project open, use File > Open and open the build.gradle file.
- If you don't have a project open (e.g. this is your first project), click "Open" in the right side of the "Welcome to IntelliJ IDEA" window.
- When importing, make sure that "use gradle wrapper" is selected.
- IntelliJ should then think for a little while and open your project.

## Making changes from here

You'll probably want to add more classes in `src/main/java` and tests in `src/test/java`. To add dependencies, [include them in your `build.gradle`](https://docs.gradle.org/current/userguide/artifact_dependencies_tutorial.html).

If you make changes to your build.gradle file, make sure to have IntelliJ re-import from gradle by going to the Gradle tab (on the right of the IntelliJ window) and clicking Refresh (the little blue arrows-in-a-circle).

You can always build the project (which includes running the tests) by running `./gradlew build`.

# I want to make this myself

If you want to create this sort of project from scratch rather than using my pre-made copy in this repo, follow these instructions.

## Set up a git repo
All coding happens in a vcs (version control system). Git is as good a choice as any, so we'll use that.
```
mkdir your-project-name
cd your-project-name
git init
```

From now on, all commands assume that you'll be in your `your-project-name` directory.

## Add a `.gitignore` so you don't track files that shouldn't be in vcs

Put this in a file named `.gitignore`:

```
# Intellij
.idea
*.iml

# Gradle
.gradle
build

# Other
.DS_Store
.directory
```

## Download gradle

[Gradle](http://gradle.org/) is the build tool of choice. We'll be using gradle to generate the [gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html). The gradle wrapper means that from now on, you (and your teammates) won't have to install gradle to build your project; the files needed to bootstrap gradle will be baked into the project itself. This also means that your build tool will be versioned along with your code, which is fantastic.

If you're on Mac or Linux, or you have a shell set up on Windows, install [gvm aka sdkman](http://sdkman.io/) and install gradle with:

```
sdk install gradle
```

If you're on Windows without a working shell (which is most Windows users), [download the Gradle zip](http://gradle.org/) and unzip it. It doesn't much matter where you put it; just remember where you unzipped it to.

Or, you can always install gradle manually; see the gradle docs.

## Generate the gradle wrapper

In your project folder, run:

```
gradle wrapper
```

If you're on Windows, instead of `gradle` you'll need to use the full path to `gradle.bat` in the `bin` dir of your unzipped copy of Gradle.

## Create a `build.gradle` file

The `build.gradle` file defines how Gradle will build your project. In this case, we'll create a basic Java project and pull in a couple of dependencies just to show how to do it.

Put this in a file named `build.gradle`:


```
apply plugin: 'java'

repositories {
  jcenter()
}

dependencies {
  compile 'com.google.code.findbugs:jsr305:3.0.0'
  testCompile 'junit:junit:4.12'
}

sourceCompatibility = JavaVersion.VERSION_1_8
```

At this point, you should be able to run `./gradlew build` (or `./gradlew.bat build` on Windows) and have it work. If you have proxy issues, keep reading!

## Create initial commit

Commit the current state of the repo:

```
git add .
git commit -m "Initial commit"
```

## Write some code

You don't have any directories to put code in yet, so make them:

```
mkdir -p src/main/java src/test/java
```

At this point, pick up the tutorial above at the section "Import the project into IntelliJ".
