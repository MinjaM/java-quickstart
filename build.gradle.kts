plugins {
    java
    id("com.github.ben-manes.versions") version "0.21.0"
}

repositories {
    jcenter()
}

val deps by extra {
    mapOf(
            "junit" to "5.4.2"
    )
}

dependencies {
    implementation("com.google.code.findbugs:jsr305:3.0.2")

    testImplementation("org.junit.jupiter:junit-jupiter-api:${deps["junit"]}")
    testImplementation("org.junit.jupiter:junit-jupiter-params:${deps["junit"]}")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:${deps["junit"]}")
}

tasks.test {
    useJUnitPlatform()
}

java {
    sourceCompatibility = JavaVersion.VERSION_11
}
